//
//  SearchResult.swift
//  StoreSearch
//
//  Created by tatiana arcos prats on 5/12/17.
//  Copyright © 2017 Condor Labs. All rights reserved.
//

import UIKit
import Foundation

private let typeForKind = [
    "album": NSLocalizedString("Album", comment: "Localized kind: Album"),
    "audiobook": NSLocalizedString("Audio book", comment: "Localized kind: Audio book"),
    "book": NSLocalizedString("Book", comment: "Localized kind: Book"),
    "ebook": NSLocalizedString("E-Book", comment: "Localized kind: E-Book"),
    "feature-movie": NSLocalizedString("Movie", comment: "Localized kind: Movie"),
    "music-video": NSLocalizedString("Music Video", comment: "Localized kind: Music Video"),
    "podcast": NSLocalizedString("Podcast", comment: "Localized kind: Podcast"),
    "software": NSLocalizedString("App", comment: "Localized kind: Software"),
    "song": NSLocalizedString("Song", comment: "Localized kind: Song"),
    "tv-episode": NSLocalizedString("TV Episode", comment: "Localized kind: TV Episode")
]

class ResultArray: Codable {
    var resultCount = 0
    var results = [SearchResult]()
    
}

class SearchResult: Codable, CustomStringConvertible {
    var artistName = ""
    var currency = ""
    var imageSmall = ""
    var imageLarge = ""
    var trackName: String?
    var kind: String?
    var trackPrice: Double?
    var trackViewUrl: String?
    var collectionName: String?
    var collectionViewUrl: String?
    var collectionPrice: Double?
    var itemPrice: Double?
    var itemGenre: String?
    var bookGenre: [String]?
    
    enum CodingKeys: String, CodingKey {
        case imageSmall = "artworkUrl60"
        case imageLarge = "artworkUrl100"
        case itemPrice = "price"
        case itemGenre = "primaryGenreName"
        case bookGenre = "genres"
        case artistName, currency, kind
        case trackPrice, trackViewUrl, trackName
        case collectionName, collectionViewUrl, collectionPrice
    }
    
    var name: String {
        return trackName ?? collectionName ?? ""
    }
    
    var storeURL: String {
        return trackViewUrl ?? collectionViewUrl ?? ""
    }
    
    var price: Double {
        return trackPrice ?? collectionPrice ?? 0.0
    }
    
    var genre: String {
        if let genre = itemGenre {
            return genre
        } else if let genres = bookGenre { // If is e-books
            return genres.joined(separator:  ",")
        } else {
            return ""
        }
    }
    
    var type: String {
        let kind = self.kind ?? "audiobook"
        return typeForKind[kind] ?? kind
    }
    
    var description: String { // kind ?? "" -> unwraps kind if it has a value, if not, it returns ""
        return "Kind: \(kind ?? ""), Name: \(name), Artist Name: \(artistName), Genre: \(genre)"
    }
}
