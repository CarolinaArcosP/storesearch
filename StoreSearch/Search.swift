//
//  Search.swift
//  StoreSearch
//
//  Created by Prats on 28/01/18.
//  Copyright © 2018 Condor Labs. All rights reserved.
//

import Foundation
import UIKit

class Search {
    enum Categoty: Int {
        case all = 0
        case music = 1
        case software = 2
        case ebooks = 3
        
        var type: String {
            switch self {
            case .all: return ""
            case .music: return "musicTrack"
            case .software: return "software"
            case .ebooks: return "ebook"
            }
        }
    }
    
    enum State {
        case notSearchedYet //also used when there's an error
        case loading
        case noResults
        case results([SearchResult]) //associated value -> array of SearchResult ojects
    }
    
    typealias SearchComplete = (Bool) -> Void //Closure that takes a Bool parameter and returns no value
    
    private(set) var state: State = .notSearchedYet //other objects are only allowed to read the state value
    private var dataTask: URLSessionDataTask? = nil
    
    func performSearch(for text: String, category: Categoty, completion: @escaping SearchComplete) {
        if !text.isEmpty {
            dataTask?.cancel() //if there's an active data task, cancels it --> no old searches can ever get in the way of the new search
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            state = .loading
            
            /*let queue =  DispatchQueue.global()
             let url = self.itunesURL(searchText: searchBar.text!)
             queue.async {
             if let data = self.performStoreRequest(with: url) {     //Request
             self.searchResults = self.parse(data: data)         //Decode
             self.searchResults.sort(by: {result1, result2 in    //Sort results
             return result1.name.localizedStandardCompare(result2.name) == .orderedAscending
             })
             DispatchQueue.main.async {
             self.isLoading = false
             self.tableView.reloadData()
             }
             return
             }
             }*/
            
            let url = itunesURL(searchText: text, category: category)
            let session = URLSession.shared
            dataTask = session.dataTask(with: url, completionHandler: {data, response, error in //completionHandler is invoked when the data task has received a responde from the server
                var newState = State.notSearchedYet
                var success = false
                if let error = error as NSError?, error.code == -999 {
                    return //Search was cancelled
                } else if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let data = data {
                    var searchResults = self.parse(data: data)
                    if searchResults.isEmpty {
                        newState = .noResults
                    } else {
                        searchResults.sort(by: {result1, result2 in
                            return result1.name.localizedStandardCompare(result2.name) == .orderedAscending
                        })
                        newState = .results(searchResults)
                    }
                    success = true
                    print("Success!")
                }
                DispatchQueue.main.async {
                    self.state = newState
                    completion(success)
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
            })
            dataTask?.resume() //sends the request to the server on a background thread
        }
    }
    
    // MARK: - Private methods
    
    private func itunesURL(searchText: String, category: Categoty) -> URL {
        let locale = Locale.autoupdatingCurrent
        let language = locale.identifier
        let countryCode = locale.regionCode ?? "en_US"
        let kind = category.type
        
        let encodedText = searchText.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let urlString = "https://itunes.apple.com/search?term=\(encodedText)&limit=200&entity=\(kind)&lang=\(language)&country=\(countryCode)"
        let url = URL(string: urlString)
        
        print("URL: \(url!)")
        return url!
    }
    
    private func parse(data: Data) -> [SearchResult] {
        do {
            let decoder = JSONDecoder()
            let result = try decoder.decode(ResultArray.self, from: data)
            return result.results
        } catch {
            print("JSON Error: \(error)")
            return []
        }
    }
}
